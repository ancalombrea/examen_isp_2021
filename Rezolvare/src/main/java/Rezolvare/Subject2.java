package Rezolvare;

import javax.swing.*;

public class Subject2 {
    public static void main(String[] args) {
        new Window();
    }
}

class Window extends JFrame {

    private final JTextField text1;
    private final JTextField text2;

    public Window() {

        this.setLayout(null);
        this.setSize(300, 300);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle("Subject 2");

        text1 = new JTextField();
        text1.setBounds(50, 50, 150, 20);
        text2 = new JTextField();
        text2.setBounds(50, 100, 150, 20);

        JButton button = new JButton("Calculate!");
        button.setBounds(80, 150, 100, 20);
        button.addActionListener(e -> {
            int Nr;
            Nr = text1.getText().length();
            String[] t = text1.getText().split(" ", Nr);
            Nr = 0;
            for (String a : t) {
                System.out.println(a + "   ");
                Nr = Nr + a.length();
            }
            text2.setText(String.valueOf(Nr));
        });

        this.add(text1);
        this.add(text2);
        this.add(button);
        this.setVisible(true);
    }
}
