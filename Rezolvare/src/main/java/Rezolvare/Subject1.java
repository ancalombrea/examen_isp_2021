package Rezolvare;

public class Subject1 {
}


class A {

}

class B extends A {
    private String param;
    D d;  // asociere
    C c;
    E e = new E();   // agregare

    public void x() {
        c = new C();   // compozitie
    }

    public void y(E e) {
    }
}

class C {

}

class D {

}

class E {

}

class U {
    public void method(B b) {  // dependenta

    }
}
